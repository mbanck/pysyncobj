Source: pysyncobj
Section: python
Priority: optional
Maintainer: Michael Banck <mbanck@debian.org>
Build-Depends: debhelper-compat (= 10),
               dh-python,
               python3-all-dev (>= 3.6),
               python3-cryptography,
               python3-pytest,
               python3-setuptools
Standards-Version: 4.4.1
X-Python3-Version: >= 3.6
Homepage: https://github.com/bakwc/PySyncObj
Vcs-Browser: https://salsa.debian.org/mbanck/pysyncobj
Vcs-Git: https://salsa.debian.org/mbanck/pysyncobj.git
Testsuite: autopkgtest-pkg-python

Package: python3-pysyncobj
Architecture: all
Section: python
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
         python3-cryptography
Description: RAFT-based python class replication between multiple servers
 PySyncObj is a python library for building fault-tolerant distributed systems.
 It provides the ability to replicate your application data between multiple
 servers. It has following features:
 .
  * raft protocol for leader election and log replication
  * Log compaction - it use fork for copy-on-write while serializing data on
    disk
  * Dynamic membership changes - you can do it with syncobj_admin utility or
    directly from your code
  * Zero downtime deploy - no need to stop cluster to update nodes
  * In-memory and on-disk serialization - you can use in-memory mode for small
    data and on-disk for big one
  * Encryption - you can set password and use it in external network
  * Configurable event loop - it can works in separate thread with it's own
    event loop - or you can call onTick function inside your own one
  * Convenient interface - one can easily transform arbitrary class into a
    replicated one
